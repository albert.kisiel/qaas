FROM python:3.11-slim

# Tell apt-get we're never going to be able to give manual feedback (on build)
ARG DEBIAN_FRONTEND=noninteractive

# Try to setup proper user IDs
ARG USER_UID=${USER_UID:-999}

# set ipdb as default debugger
# tell python not to buffer stdout
# tell python not to write bytecode to disk
ENV PYTHONBREAKPOINT=ipdb.set_trace \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1

## os dependencies
#RUN apt-get update -y && \
#    apt-get -y install --no-install-recommends build-essential postgresql-client netcat libgdal-dev && \
#    apt-get clean && \
#    rm -rf /var/lib/apt/lists/*

# user
RUN adduser --system --uid=${USER_UID} --group --home=/qaas-api/  --shell=/sbin/nologin --gecos "Docker image user" qaas

# copy additional configs
COPY --chown=qaas:qaas pyproject.toml /app/pyproject.toml
COPY --chown=qaas:qaas pytest.ini /app/pytest.ini

# install requirements
COPY --chown=qaas:qaas requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt


# copy code
COPY --chown=qaas:qaas qaas_api /app/qaas_api

USER qaas

WORKDIR /app/qaas_api
