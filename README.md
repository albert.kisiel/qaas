# qaas_api

Django Application created for Oper Team as recruitment task.

Based on python 3.11 and Django 4.2. Docker and docker-compose are recommended in order to run the app.

## Assumptions made during development
#### Technical
- I decided to not use PostgreSQL/Redis/Celery to not overcomplicate the app.
- Settings/configs are prepared in "local development" in mind
- No need for setting environment variables (at least if you'll run the app using docker-compose)
- The app uses console email backend i.e. all emails supposed to be sent will be printed in the console
- JWT auth has been used for the endpoints
- Lifetime for JWT tokens has been extended for purpose of easier testng - access token 7 days, refresh token 30 days
- Use following header to authorize your requests: `Authorization: Bearer <your-token>`

#### Business logic related
- User must be authenticated in order to accept invitation
- All users are free to register and start creating their own quizzes
- Question scoring is binary i.e. user have to provide all answers flagged as correct in order to get a point
- All admin interface related requirements are available from django's admin panel


## Local development
User docker-compose to build and run project:
```shell
docker-compose build --force-rm
docker-compose up
```
#### Useful commands
- Create superuser
```shell
- docker-compose run --rm  api python manage.py createsuperuser
```
- Log into container
```shell
docker-compose run --rm  api sh
```
- Run tests
```shell
docker-compose run --rm  api pytest
```

- Obtain JWT access token
```shell
curl \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{"email": "admin@admin.com", "password": "admin"}' \
  http://localhost:8000/api/v1/token/
```

## Interactive API docs urls:
- http://localhost:8000/swagger/
- http://localhost:8000/redoc/

## Admin interface (Django admin):
- http://localhost:8000/admin/

## Brief endopints description
- Endpoints that starts with `/api/v1/management` are meant to be used by quiz creators i.e. managing their own quizzes
- Endpoints that starts with `/api/v1/invitations` (except `/invitations/accept/{token}/`) are meant to be used by quiz creators for invitation man
- Endpoints that starts with `/api/v1/participant` are meant to be used by quiz participants i.e. checking their progress, reading/creating provided answers

## Admin requirements description
- Models are configured to be editable
- In order to generate usage report (UsageReport model) for current day, I extended the admin template. Use `Generate usage report for today` visible over actions dropdown menu
- Usage reports are available for downloading by using `Export selected usage reports` action. Note that file format have to be selected from additional dropdown menu.


Docs are done, it's best time to run code formatter on all files.

Best regards, Albert
