import factory

from apps.quizzes.models import QuizParticipant


class QuizParticipantFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory("apps.quizzes.tests.factories.quiz_users.QuizUserFactory")
    quiz = factory.SubFactory("apps.quizzes.tests.factories.quizzes.QuizWithoutParticipantsFactory")

    class Meta:
        model = QuizParticipant
