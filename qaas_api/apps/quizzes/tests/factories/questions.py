import factory

from apps.quizzes.models import Question
from apps.quizzes.tests.factories.answers import CorrectAnswerFactory, IncorrectAnswerFactory


class QuestionFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: "Title question #%s" % n)
    quiz = factory.SubFactory("apps.quizzes.tests.factories.quizzes.QuizWithParticipantsFactory")

    class Meta:
        model = Question
        skip_postgeneration_save = True

    @factory.post_generation
    def answers(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.answers.add(*extracted)
        else:
            self.answers.add(*[CorrectAnswerFactory.create(question=self) for _ in range(2)])
            self.answers.add(*[IncorrectAnswerFactory.create(question=self) for _ in range(2)])
