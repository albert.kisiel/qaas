import factory
from factory import fuzzy

from apps.users.models import QuizUser


class QuizUserFactory(factory.django.DjangoModelFactory):
    email = factory.Faker("safe_email")
    password = fuzzy.FuzzyText(length=12)
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")

    class Meta:
        model = QuizUser
