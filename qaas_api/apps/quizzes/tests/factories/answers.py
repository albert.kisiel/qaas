import factory

from apps.quizzes.models import Answer


class CorrectAnswerFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: "Title answer #%s" % n)
    is_correct = True
    question = factory.SubFactory("apps.quizzes.tests.factories.questions.QuestionFactory")

    class Meta:
        model = Answer


class IncorrectAnswerFactory(factory.django.DjangoModelFactory):
    is_correct = False

    class Meta:
        model = Answer
