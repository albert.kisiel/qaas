import factory

from apps.quizzes.models import Quiz, QuizStatusChoices
from apps.quizzes.tests.factories.questions import QuestionFactory
from apps.quizzes.tests.factories.quiz_users import QuizUserFactory


class QuizWithoutParticipantsFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: "Title quiz #%s" % n)
    description = factory.Sequence(lambda n: "Description #%s" % n)
    status = QuizStatusChoices.ACTIVE
    author = factory.SubFactory("apps.quizzes.tests.factories.quiz_users.QuizUserFactory")

    class Meta:
        model = Quiz
        skip_postgeneration_save = True

    @factory.post_generation
    def questions(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.questions.add(*extracted)
        else:
            self.questions.add(*[QuestionFactory.create(quiz=self) for _ in range(3)])


class QuizWithParticipantsFactory(QuizWithoutParticipantsFactory):
    @factory.post_generation
    def participants(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.participants.add(*extracted)
        else:
            self.participants.add(*QuizUserFactory.create_batch(3))
