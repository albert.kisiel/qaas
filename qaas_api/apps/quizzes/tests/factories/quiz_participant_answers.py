import factory

from apps.quizzes.models import QuizParticipantAnswer


class QuizParticipantAnswerFactory(factory.django.DjangoModelFactory):
    participant = factory.SubFactory("apps.quizzes.tests.factories.quiz_participants.QuizParticipantFactory")
    question = factory.LazyAttribute(lambda obj: obj.participant.quiz.questions.first())

    class Meta:
        model = QuizParticipantAnswer
        skip_postgeneration_save = True


class QuizParticipantAnswerAllAnswersCorrect(QuizParticipantAnswerFactory):
    @factory.post_generation
    def answers(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.answers.add(*extracted)
        else:
            self.answers.add(*self.question.answers.filter(is_correct=True))


class QuizParticipantAnswerAllAnswersIncorrect(QuizParticipantAnswerFactory):
    @factory.post_generation
    def answers(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.answers.add(*extracted)
        else:
            self.answers.add(*self.question.answers.filter(is_correct=False))


class QuizParticipantAnswerMixedAnswers(QuizParticipantAnswerFactory):
    @factory.post_generation
    def answers(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.answers.add(*extracted)
        else:
            self.answers.add(*self.question.answers.all())
