import pytest

from rest_framework.exceptions import ValidationError

from apps.quizzes.serializers import QuizParticipantAnswerSerializer

pytestmark = [pytest.mark.django_db]


class DummyRequest:
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)


class TestQuizParticipantAnswerSerializer:
    def test_creation_happy_path(self, quiz_participant):
        """
        i.e. users tries to provide answers for question where:
        - provided answers are subset of question.answers.all()
        - related participation exists for user sending request
        """
        answered_questions_qs = quiz_participant.answered_questions
        answered_questions_count = answered_questions_qs.count()
        assert answered_questions_count == 0
        assert quiz_participant.quiz.questions.count() == 3
        question = quiz_participant.quiz.questions.first()
        assert question.answers.count() == 4

        serializer = QuizParticipantAnswerSerializer(
            data={"question": question.pk, "answers": [question.answers.first().pk]},
            context={"request": DummyRequest(user=quiz_participant.user)},
        )

        assert serializer.is_valid(raise_exception=False)
        serializer.save()
        assert answered_questions_count + 1 == answered_questions_qs.count()

    def test_creation_no_participation_for_provided_question(self, quiz_participant, question):
        """
        There must be QuizParticipation related to Question that user is providing the answers,
        otherwise ValidationError will be raised
        """
        answered_questions_qs = quiz_participant.answered_questions
        answered_questions_count = answered_questions_qs.count()
        assert answered_questions_count == 0
        assert quiz_participant.quiz.questions.filter(id__in=[question.pk]).count() == 0

        serializer = QuizParticipantAnswerSerializer(
            data={
                # unrelated question from different fixture
                "question": question.pk,
                "answers": [question.answers.first().pk],
            },
            context={"request": DummyRequest(user=quiz_participant.user)},
        )

        with pytest.raises(ValidationError, match="No participation with provided question exists for the user"):
            serializer.is_valid(raise_exception=True)

        # no question answers has been added
        assert answered_questions_count == answered_questions_qs.count()

    def test_creation_with_unrelated_answers(self, quiz_participant, correct_answer):
        """
        If provided answers aren't related to the provided question,
        ValidationError will be raised
        """
        answered_questions_qs = quiz_participant.answered_questions
        answered_questions_count = answered_questions_qs.count()
        assert answered_questions_count == 0
        question = quiz_participant.quiz.questions.first()

        serializer = QuizParticipantAnswerSerializer(
            data={
                "question": question.pk,
                # unrelated answer from different fixture
                "answers": [correct_answer.pk],
            },
            context={"request": DummyRequest(user=quiz_participant.user)},
        )

        with pytest.raises(ValidationError, match="Provided answers don't belong to given question"):
            serializer.is_valid(raise_exception=True)

        # no question answers has been added
        assert answered_questions_count == answered_questions_qs.count()
