import pytest

from apps.quizzes.serializers import QuizParticipantProgressSerializer

pytestmark = [pytest.mark.django_db]


class TestQuizParticipantProgressSerializer:
    def test_score_if_all_provided_answers_are_correct(self, quiz_participant_answer_all_correct):
        """
        If all provided answers are correct:
        - obj should have "all_answers_correct" flag
        - final score should be 1
        """
        serializer = QuizParticipantProgressSerializer(instance=quiz_participant_answer_all_correct.participant)
        data = serializer.data
        assert len(data["answered_questions"]) == 1
        assert data["answered_questions"][0]["all_answers_are_correct"]
        assert all(
            answer["is_correct"]
            for answered_question in data["answered_questions"]
            for answer in answered_question["answers"]
        )
        assert data["score"] == 1

    def test_score_if_all_provided_answers_are_partially_correct(self, quiz_participant_answer_all_correct):
        """
        If all provided answers are correct but not all correct answers has been provided:
        - obj should have "all_answers_correct" flag == False
        - final score should be 0
        """
        # remove 1 of 2 correct answers
        answer_to_remove = quiz_participant_answer_all_correct.answers.first()
        quiz_participant_answer_all_correct.answers.remove(answer_to_remove)
        serializer = QuizParticipantProgressSerializer(instance=quiz_participant_answer_all_correct.participant)
        data = serializer.data
        assert len(data["answered_questions"]) == 1
        assert data["answered_questions"][0]["all_answers_are_correct"] is False
        assert all(
            answer["is_correct"]
            for answered_question in data["answered_questions"]
            for answer in answered_question["answers"]
        )
        assert data["score"] == 0

    def test_score_if_all_provided_answers_are_mixed(self, quiz_participant_answer_mixed):
        """
        If all provided answers are mixed e.g some are correct, some aren't:
        - obj should have "all_answers_correct" flaa == False
        - final score should be 0
        """
        serializer = QuizParticipantProgressSerializer(instance=quiz_participant_answer_mixed.participant)
        data = serializer.data
        assert len(data["answered_questions"]) == 1
        assert data["answered_questions"][0]["all_answers_are_correct"] is False
        # breakpoint()
        assert any(
            answer["is_correct"]
            for answered_question in data["answered_questions"]
            for answer in answered_question["answers"]
        )
        assert any(
            answer["is_correct"] is False
            for answered_question in data["answered_questions"]
            for answer in answered_question["answers"]
        )
        assert data["score"] == 0

    def test_score_if_all_provided_answers_are_incorrect(self, quiz_participant_answer_all_incorrect):
        """
        If all provided answers are incorrect:
        - obj should have "all_answers_correct" flga == False
        - final score should be 0
        """
        serializer = QuizParticipantProgressSerializer(instance=quiz_participant_answer_all_incorrect.participant)
        data = serializer.data
        assert len(data["answered_questions"]) == 1
        assert data["answered_questions"][0]["all_answers_are_correct"] is False
        # breakpoint()
        assert not any(
            answer["is_correct"]
            for answered_question in data["answered_questions"]
            for answer in answered_question["answers"]
        )
        assert any(
            answer["is_correct"] is False
            for answered_question in data["answered_questions"]
            for answer in answered_question["answers"]
        )
        assert data["score"] == 0
