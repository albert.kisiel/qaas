import pytest

from apps.quizzes.tests.factories.questions import QuestionFactory


@pytest.fixture
def question():
    return QuestionFactory.create()
