import pytest

from apps.quizzes.tests.factories.quiz_users import QuizUserFactory


@pytest.fixture
def quiz_user():
    return QuizUserFactory.create()
