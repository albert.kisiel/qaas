import pytest

from apps.quizzes.tests.factories.quizzes import QuizWithParticipantsFactory, QuizWithoutParticipantsFactory


@pytest.fixture
def quiz_with_participants():
    return QuizWithParticipantsFactory.create()


@pytest.fixture
def quiz_without_participants():
    return QuizWithoutParticipantsFactory.create()
