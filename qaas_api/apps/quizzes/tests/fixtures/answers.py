import pytest

from apps.quizzes.tests.factories.answers import CorrectAnswerFactory, IncorrectAnswerFactory


@pytest.fixture
def correct_answer():
    return CorrectAnswerFactory.create()


@pytest.fixture
def incorrect_answer():
    return IncorrectAnswerFactory.create()
