import pytest

from apps.quizzes.tests.factories.quiz_participant_answers import (
    QuizParticipantAnswerFactory,
    QuizParticipantAnswerAllAnswersCorrect,
    QuizParticipantAnswerAllAnswersIncorrect,
    QuizParticipantAnswerMixedAnswers,
)


@pytest.fixture
def quiz_participant_answer():
    return QuizParticipantAnswerFactory.create()


@pytest.fixture
def quiz_participant_answer_all_correct():
    return QuizParticipantAnswerAllAnswersCorrect.create()


@pytest.fixture
def quiz_participant_answer_all_incorrect():
    return QuizParticipantAnswerAllAnswersIncorrect.create()


@pytest.fixture
def quiz_participant_answer_mixed():
    return QuizParticipantAnswerMixedAnswers.create()
