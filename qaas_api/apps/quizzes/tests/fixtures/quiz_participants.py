import pytest

from apps.quizzes.tests.factories.quiz_participants import QuizParticipantFactory


@pytest.fixture
def quiz_participant():
    return QuizParticipantFactory.create()
