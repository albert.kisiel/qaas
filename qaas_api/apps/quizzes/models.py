import logging
import uuid

from django.conf import settings
from django.core.mail import send_mail
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from apps.quizzes.exceptions import InvitationAlreadyAcceptedError, QuizAlreadyFinishedError

logger = logging.getLogger(__name__)


class CreatedAtAbstractModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class TitleAbstractModel(models.Model):
    title = models.CharField(max_length=364)

    class Meta:
        abstract = True


class QuizStatusChoices(models.TextChoices):
    DRAFT = "draft", _("draft")
    ACTIVE = "active", _("active")
    FINISHED = "finished", _("finished")


class Quiz(TitleAbstractModel, CreatedAtAbstractModel):
    description = models.TextField(max_length=128, blank=True)
    status = models.CharField(choices=QuizStatusChoices.choices, max_length=8, default=QuizStatusChoices.DRAFT)
    author = models.ForeignKey(to="users.QuizUser", on_delete=models.CASCADE, related_name="quizzes")
    participants = models.ManyToManyField(to="users.QuizUser", through="quizzes.QuizParticipant")

    class Meta:
        verbose_name_plural = _("Quizzes")

    def __str__(self):
        return self.title

    def send_result_email(self, participant):
        # TODO: in the future, use celery for that
        from apps.quizzes.serializers import QuizParticipantProgressSerializer

        data = QuizParticipantProgressSerializer(instance=participant).data

        score = data["score"]
        questions_count = data["quiz"]["questions_count"]  # also means max score (max 1 point per question)
        answered_questions_count = len(data["answered_questions"])
        subject = f"Quiz results for {self.title}"
        message = (
            "Congratulations!\n\n"
            f"By providing answers for {answered_questions_count}/{questions_count} questions, "
            f"You've scored {score}/{questions_count}"
        )
        send_mail(subject, message, None, [participant.user.email], fail_silently=False)

    def close_and_notify_participants(self):
        if not self.status == QuizStatusChoices.ACTIVE:
            raise QuizAlreadyFinishedError

        try:
            for participant in self.participants.through.objects.filter(quiz_id=self.pk):
                self.send_result_email(participant)
            self.status = QuizStatusChoices.FINISHED
            self.save()
        except Exception as exc:
            logger.error("Failed to send result emails", exc_info=exc)


class Question(TitleAbstractModel, CreatedAtAbstractModel):
    quiz = models.ForeignKey(to=Quiz, on_delete=models.CASCADE, related_name="questions")

    class Meta:
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")

    def __str__(self):
        return f"{self.quiz}: {self.title}"


class Answer(TitleAbstractModel):
    question = models.ForeignKey(to=Question, on_delete=models.CASCADE, related_name="answers")
    is_correct = models.BooleanField()

    class Meta:
        verbose_name = _("Answer")
        verbose_name_plural = _("Answers")

    def __str__(self):
        return f"{self.question}: {self.title}"


class Invitation(models.Model):
    quiz = models.ForeignKey(to=Quiz, on_delete=models.CASCADE)
    email = models.EmailField(_("email address"))
    token = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    last_attempt_at = models.DateTimeField(
        blank=True, null=True, help_text=_("The date when last email sending attempt happened")
    )
    sent_at = models.DateTimeField(
        blank=True, null=True, help_text=_("The date when email has been successfully sent to the invitee")
    )
    accepted = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Invitation")
        verbose_name_plural = _("Invitations")
        unique_together = ("quiz", "email")

    def __str__(self):
        return f"{self.email} - {self.quiz.title}"

    def get_accept_invitation_absolute_url(self):
        path = reverse("quizzes:accept_invitation", kwargs={"token": self.token})
        return f"{settings.APP_DOMAIN_URL}{path}"

    def send_email(self):
        """
        Simple text email, no html template

        I guess that usually the email should contain a client url for handling acceptation flow
        but in this case only url for the invitation accepting endpoint will be included
        """
        if self.accepted:
            raise InvitationAlreadyAcceptedError

        author = self.quiz.author
        subject = f"'{self.quiz.title}' invitation!"
        message = (
            "Hi!\n\n"
            f"{author.get_short_name() or author.email} have invited you "
            f"to take a participation in quiz: '{self.quiz.title}'\n\n"
            f"You can accept this invitation by clicking in following link: {self.get_accept_invitation_absolute_url()}"
            "\n"
        )
        attempt_time = timezone.now()
        try:
            self.last_attempt_at = attempt_time
            send_mail(subject, message, None, [self.email])
        except Exception as exc:
            # log any exception
            logger.error("Failed to send invitation email", exc_info=exc)
        else:
            self.invitation_sent_at = attempt_time

        self.save()


class QuizParticipant(CreatedAtAbstractModel):
    user = models.ForeignKey(to="users.QuizUser", on_delete=models.CASCADE)
    quiz = models.ForeignKey(to=Quiz, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Quiz Participant")
        verbose_name_plural = _("Quiz Participants")
        unique_together = ("user", "quiz")

    def __str__(self):
        return f"{self.user.email} - {self.quiz.title}"


class QuizParticipantAnswer(CreatedAtAbstractModel):
    participant = models.ForeignKey(to=QuizParticipant, on_delete=models.CASCADE, related_name="answered_questions")
    question = models.ForeignKey(to=Question, on_delete=models.CASCADE)
    answers = models.ManyToManyField(to=Answer)

    class Meta:
        verbose_name = _("Quiz Participant Answer")
        verbose_name_plural = _("Quiz Participant Answers")
        unique_together = ("participant", "question")

    def __str__(self):
        return f"{self.participant.user.email} - {self.question.title}"
