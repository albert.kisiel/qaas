from django.contrib import admin

from apps.quizzes.models import Quiz, Answer, Question, QuizParticipant, QuizParticipantAnswer, Invitation


class AnswersFieldMixin:
    def answers(self, question):
        return ", ".join([title for title in question.answers.values_list("title", flat=True)])


class QuestionInline(AnswersFieldMixin, admin.TabularInline):
    model = Question
    readonly_fields = ("id", "title", "answers")
    extra = 0
    show_change_link = True


class AnswerInline(admin.TabularInline):
    model = Answer
    fields = ("id", "title", "is_correct")
    readonly_fields = ("id",)
    extra = 4
    show_change_link = True


@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "title",
        "description",
        "status",
        "author",
    )
    search_fields = ("id", "title", "description", "author__email", "author__first_name", "author__last_name")
    list_filter = ("status",)
    inlines = (QuestionInline,)


@admin.register(Question)
class QuestionAdmin(AnswersFieldMixin, admin.ModelAdmin):
    list_display = ("id", "title", "answers")
    inlines = (AnswerInline,)


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "question")
    search_fields = ("title", "question__title")
    list_filter = ("is_correct",)


@admin.register(Invitation)
class InvitationAdmin(admin.ModelAdmin):
    list_display = ("id", "email", "quiz", "accepted", "sent_at")
    search_fields = ("email", "quiz__title", "quiz__description")


@admin.register(QuizParticipant)
class QuizParticipantAdmin(admin.ModelAdmin):
    list_display = ("id", "user", "quiz")
    search_fields = ("id", "user__email", "user__first_name", "user__last_name", "quiz__title")


@admin.register(QuizParticipantAnswer)
class QuizParticipantAnswerAdmin(admin.ModelAdmin):
    list_display = ("id", "participant", "quiz", "question", "provided_answers")
    search_fields = (
        "id",
        "participant__user__email",
        "participant__user__first_name",
        "participant__user__last_name",
        "question__title",
        "question__quiz__title",
    )

    def quiz(self, participant_answer):
        return participant_answer.question.quiz

    def provided_answers(self, participant_answer):
        return ", ".join([title for title in participant_answer.answers.all().values_list("title", flat=True)])
