import django_filters
from django.db.models import Q

from apps.quizzes.models import (
    Quiz,
    Question,
    Answer,
    Invitation,
    QuizStatusChoices,
    QuizParticipantAnswer,
    QuizParticipant,
)


class QuizFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
    status = django_filters.ChoiceFilter(choices=QuizStatusChoices.choices)

    class Meta:
        model = Quiz
        fields = ["search", "status"]

    def search_filter(self, queryset, name, value):
        return queryset.filter(Q(title__icontains=value) | Q(description__icontains=value))


class QuestionFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")

    class Meta:
        model = Question
        fields = ["search"]

    def search_filter(self, queryset, name, value):
        return queryset.filter(title__icontains=value)


class AnswerFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
    is_correct = django_filters.BooleanFilter()

    class Meta:
        model = Answer
        fields = ["search", "is_correct"]

    def search_filter(self, queryset, name, value):
        return queryset.filter(Q(title__icontains=value) | Q(question__title__icontains=value))


class InvitationFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
    email = django_filters.CharFilter()
    quiz = django_filters.CharFilter(field_name="quiz__title")
    accepted = django_filters.BooleanFilter()
    quiz_status = django_filters.ChoiceFilter(choices=QuizStatusChoices.choices, field_name="quiz__status")

    class Meta:
        model = Invitation
        fields = ["search", "email", "quiz", "accepted", "quiz_status"]

    def search_filter(self, queryset, name, value):
        return queryset.filter(Q(email__icontains=value) | Q(quiz__title__icontains=value))


class QuizParticipantFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
    status = django_filters.ChoiceFilter(choices=QuizStatusChoices.choices, field_name="quiz__status")

    class Meta:
        model = QuizParticipant
        fields = ["search", "status"]

    def search_filter(self, queryset, name, value):
        return queryset.filter(Q(user__email__icontains=value) | Q(quiz__title__icontains=value))


class QuizParticipantAnswerFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
    participant = django_filters.CharFilter(field_name="participant__user__email")

    class Meta:
        model = QuizParticipantAnswer
        fields = ["search", "participant"]

    def search_filter(self, queryset, name, value):
        return queryset.filter(
            Q(participant__user__email=value) | Q(question__title__icontains=value) | Q(answers__title__icontains=value)
        )
