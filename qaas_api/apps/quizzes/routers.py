from rest_framework.routers import DefaultRouter

from apps.quizzes.views import (
    AnswerManagementViewSet,
    InvitationViewSet,
    QuestionManagementViewSet,
    QuizManagementViewSet,
    QuizParticipantAnswerViewSet,
    QuizParticipantReadOnlyViewSet,
    QuizParticipantReadOnlyManagementViewSet,
)

invitations_router = DefaultRouter()
invitations_router.register(r"", InvitationViewSet, basename="invitations")

management_router = DefaultRouter()
management_router.register(r"quizzes", QuizManagementViewSet, basename="quizzes_management")
management_router.register(r"questions", QuestionManagementViewSet, basename="questions_management")
management_router.register(r"answers", AnswerManagementViewSet, basename="answers_management")
management_router.register("quizzes-progress", QuizParticipantReadOnlyManagementViewSet, "quizzes_progress_management")

participant_router = DefaultRouter()
participant_router.register(r"quizzes-progress", QuizParticipantReadOnlyViewSet, basename="progress_participant")
participant_router.register(r"answers", QuizParticipantAnswerViewSet, basename="answers_participant")
