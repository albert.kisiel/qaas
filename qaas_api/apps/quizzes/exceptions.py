from django.utils.translation import gettext_lazy as _

from rest_framework import status
from rest_framework.exceptions import APIException


class InvitationAlreadyAcceptedError(Exception):
    pass


class InvitationAlreadyAcceptedAPIError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _("Invitation already accepted")


class QuizAlreadyFinishedError(Exception):
    pass


class QuizAlreadyFinishedAPIError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _("This quiz has been already finished and results has been sent to participants")
