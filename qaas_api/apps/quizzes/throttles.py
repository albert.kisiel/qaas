from rest_framework.throttling import SimpleRateThrottle


class ResendEmailThrottle(SimpleRateThrottle):
    rate = "1/m"

    def get_cache_key(self, request, view):
        return self.cache_format % {
            "scope": view.request.path,
            "ident": request.user.pk,
        }
