from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, ListModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from apps.quizzes.exceptions import InvitationAlreadyAcceptedError, InvitationAlreadyAcceptedAPIError
from apps.quizzes.filters import InvitationFilter
from apps.quizzes.models import Invitation, QuizParticipant
from apps.quizzes.serializers import InvitationSerializer
from apps.quizzes.throttles import ResendEmailThrottle


class InvitationViewSet(CreateModelMixin, RetrieveModelMixin, ListModelMixin, GenericViewSet):
    """
    Invitation management view set for quiz creator
    """

    model = Invitation
    serializer_class = InvitationSerializer
    filterset_class = InvitationFilter

    def get_queryset(self):
        return Invitation.objects.filter(quiz__author=self.request.user)

    def perform_create(self, serializer):
        obj = serializer.save()

        # TODO: use celery for sending emails in the future
        obj.send_email()

    @swagger_auto_schema(
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT),
        responses={
            200: openapi.Schema(
                type=openapi.TYPE_OBJECT, properties={"status": openapi.Schema(type=openapi.TYPE_STRING, default="ok")}
            ),
        },
    )
    @action(methods=["post"], detail=True, throttle_classes=[ResendEmailThrottle])
    def resend(self, request, pk=None):
        """
        Resend invitation in case if first invitation somehow never reached the user or if it failed somehow

        Max 1 resend request per minute for given invitation.
        """
        obj = self.get_object()
        try:
            obj.send_email()
        except InvitationAlreadyAcceptedError:
            raise InvitationAlreadyAcceptedAPIError
        return Response({"status": "ok"}, status=status.HTTP_200_OK)


class AcceptInvitationView(APIView):
    def post(self, request, token):
        invitation = get_object_or_404(Invitation, token=token)

        if invitation.accepted:
            return Response({"error": "Invitation is already accepted"}, status=status.HTTP_400_BAD_REQUEST)

        if invitation.email != request.user.email:
            return Response(
                {"error": "Invalid account for accepting the invitation"}, status=status.HTTP_401_UNAUTHORIZED
            )

        QuizParticipant.objects.create(user=request.user, quiz=invitation.quiz)
        invitation.accepted = True
        invitation.save()
        return Response(status=status.HTTP_200_OK)
