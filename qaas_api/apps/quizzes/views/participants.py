__doc__ = """
Views to be used by quiz participants for reviewing their progress and creating/updating/listing answers for quizzes
"""

from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, ListModelMixin
from rest_framework.viewsets import GenericViewSet, ReadOnlyModelViewSet

from apps.quizzes.filters import QuizParticipantFilter, QuizParticipantAnswerFilter
from apps.quizzes.models import QuizParticipant, QuizParticipantAnswer, QuizStatusChoices
from apps.quizzes.serializers import QuizParticipantProgressSerializer, QuizParticipantAnswerSerializer


class QuizParticipantReadOnlyViewSet(ReadOnlyModelViewSet):
    """
    Show progress check for all quiz participantions i.e. all quizzes the user has been invited
    """

    serializer_class = QuizParticipantProgressSerializer
    filterset_class = QuizParticipantFilter

    def get_queryset(self):
        return QuizParticipant.objects.filter(user=self.request.user)


class QuizParticipantAnswerViewSet(
    CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, ListModelMixin, GenericViewSet
):
    serializer_class = QuizParticipantAnswerSerializer
    filterset_class = QuizParticipantAnswerFilter

    def get_queryset(self):
        qs = QuizParticipantAnswer.objects.filter(participant__user=self.request.user)
        if self.action in ("create", "update", "partial_update"):
            qs = qs.filter(participant__quiz__status=QuizStatusChoices.ACTIVE)
        return qs
