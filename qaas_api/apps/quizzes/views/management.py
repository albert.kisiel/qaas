__doc__ = """
Management views for quiz creators

Views provides control on building quizzes 
and progress preview for users that takes participation in created quizzes
"""

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from apps.quizzes.exceptions import QuizAlreadyFinishedError, QuizAlreadyFinishedAPIError
from apps.quizzes.filters import QuizFilter, QuestionFilter, AnswerFilter, QuizParticipantFilter
from apps.quizzes.models import Quiz, Question, Answer, QuizParticipant
from apps.quizzes.serializers import (
    AnswerSerializer,
    QuestionSerializer,
    QuizParticipantProgressSerializer,
    QuizSerializer,
)
from apps.quizzes.throttles import ResendEmailThrottle


class QuizManagementViewSet(ModelViewSet):
    serializer_class = QuizSerializer
    filterset_class = QuizFilter

    def get_queryset(self):
        return Quiz.objects.filter(author=self.request.user)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @swagger_auto_schema(
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT),
        responses={
            200: openapi.Schema(
                type=openapi.TYPE_OBJECT, properties={"status": openapi.Schema(type=openapi.TYPE_STRING, default="ok")}
            ),
        },
    )
    @action(methods=["post"], detail=True, url_path="close-and-send-results", throttle_classes=[ResendEmailThrottle])
    def close_and_send_results(self, request, pk=None):
        """
        Change quiz status from active -> finished,
        And send emails for all participants with their results
        """
        obj = self.get_object()
        try:
            obj.close_and_notify_participants()
        except QuizAlreadyFinishedError:
            raise QuizAlreadyFinishedAPIError
        return Response({"status": "ok"}, status=status.HTTP_200_OK)


class QuestionManagementViewSet(ModelViewSet):
    serializer_class = QuestionSerializer
    filterset_class = QuestionFilter

    def get_queryset(self):
        return Question.objects.filter(quiz__author=self.request.user)


class AnswerManagementViewSet(ModelViewSet):
    serializer_class = AnswerSerializer
    filterset_class = AnswerFilter

    def get_queryset(self):
        return Answer.objects.filter(question__quiz__author=self.request.user)


class QuizParticipantReadOnlyManagementViewSet(ReadOnlyModelViewSet):
    """
    Retrieve progress check for all users that participates in quizzes created by request user
    """

    serializer_class = QuizParticipantProgressSerializer
    filterset_class = QuizParticipantFilter

    def get_queryset(self):
        created_quizzes_ids = self.request.user.quizzes.values_list("id", flat=True)
        return QuizParticipant.objects.filter(quiz__id__in=created_quizzes_ids)
