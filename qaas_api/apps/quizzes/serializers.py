from django.utils.translation import gettext_lazy as _

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.quizzes.models import Quiz, Question, Answer, Invitation, QuizParticipant, QuizParticipantAnswer
from apps.users.serializers import QuizUserRetrieveSerializer


class QuizSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ["id", "title", "description", "status"]
        model = Quiz


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ["id", "title", "quiz"]
        model = Question


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ["id", "title", "question", "is_correct"]
        model = Answer


class InvitationSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ["id", "email", "quiz", "last_attempt_at", "sent_at", "accepted"]
        read_only_fields = ["last_attempt_at", "sent_at", "accepted"]
        model = Invitation


class QuizProgressSerializer(serializers.ModelSerializer):
    questions_count = serializers.SerializerMethodField(help_text=_("Number of questions available for the quiz"))

    class Meta:
        fields = ["id", "title", "description", "status", "questions_count"]
        model = Quiz

    def get_questions_count(self, obj):
        return obj.questions.count()


class QuizParticipantAnswerSerializer(serializers.ModelSerializer):
    participant = serializers.HiddenField(default=None)

    class Meta:
        fields = ["id", "question", "answers", "participant"]
        model = QuizParticipantAnswer

    def validate(self, attrs):
        request = self.context["request"]
        answers = attrs["answers"]
        question = attrs["question"]
        quiz = Quiz.objects.filter(questions__in=[question]).first()

        try:
            participant = QuizParticipant.objects.get(user=request.user, quiz=quiz)
        except QuizParticipant.DoesNotExist:
            raise ValidationError(_("No participation with provided question exists for the user"))

        question_answers = set(question.answers.values_list("id", flat=True))
        provided_answers = set([answer.id for answer in answers])
        if not provided_answers.issubset(question_answers):
            raise ValidationError(_("Provided answers don't belong to given question"))

        attrs["participant"] = participant
        return attrs


class QuizParticipantAnswerProgressSerializer(serializers.ModelSerializer):
    question = QuestionSerializer()
    answers = AnswerSerializer(many=True)
    all_answers_are_correct = serializers.SerializerMethodField(
        help_text=_("If true, all provided answers are correct and deserves for a score point")
    )

    class Meta:
        fields = ["id", "question", "answers", "all_answers_are_correct"]
        model = QuizParticipantAnswer

    def get_all_answers_are_correct(self, obj):
        provided_answers = obj.answers.values_list("id", flat=True)
        correct_answers = obj.question.answers.filter(is_correct=True).values_list("id", flat=True)
        return set(provided_answers) == set(correct_answers)


class QuizParticipantProgressSerializer(serializers.ModelSerializer):
    quiz = QuizProgressSerializer()
    user = QuizUserRetrieveSerializer()
    answered_questions = QuizParticipantAnswerProgressSerializer(many=True)
    unanswered_questions = serializers.SerializerMethodField()
    score = serializers.SerializerMethodField()

    class Meta:
        fields = ["id", "quiz", "user", "answered_questions", "unanswered_questions", "score"]
        model = QuizParticipant

    def get_unanswered_questions(self, obj):
        answered_questions_ids = obj.answered_questions.values_list("question__id", flat=True)
        qs = Question.objects.exclude(id__in=answered_questions_ids)
        return QuestionSerializer(qs, many=True).data

    def get_score(self, obj):
        answered_questions_data = QuizParticipantAnswerProgressSerializer(obj.answered_questions, many=True).data
        return sum(data.get("all_answers_are_correct", False) for data in answered_questions_data)
