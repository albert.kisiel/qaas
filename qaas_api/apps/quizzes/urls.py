from django.urls import path, include

from apps.quizzes.routers import invitations_router, management_router, participant_router
from apps.quizzes.views import AcceptInvitationView

app_name = "quizzes"

urlpatterns = [
    path("invitations/", include(invitations_router.urls)),
    path("invitations/accept/<uuid:token>/", AcceptInvitationView.as_view(), name="accept_invitation"),
    path("management/", include(management_router.urls)),
    path("participant/", include(participant_router.urls)),
]
