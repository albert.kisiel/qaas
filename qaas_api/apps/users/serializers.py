from rest_framework.serializers import ModelSerializer

from apps.users.models import QuizUser


class QuizUserRegistrationSerializer(ModelSerializer):
    class Meta:
        model = QuizUser
        fields = ["email", "password", "first_name", "last_name"]

    def create(self, validated_data):
        password = validated_data.pop("password")
        user = self.Meta.model(**validated_data)
        user.set_password(password)
        user.save()
        return user


class QuizUserRetrieveSerializer(ModelSerializer):
    class Meta:
        model = QuizUser
        fields = ["id", "email", "first_name", "last_name"]
