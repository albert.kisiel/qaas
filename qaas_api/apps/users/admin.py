from django.contrib import admin

from apps.users.models import QuizUser


@admin.register(QuizUser)
class QuizUserAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "email",
        "is_active",
        "first_name",
        "last_name",
    )
    exclude = ("password", "credentials")
    search_fields = ("email", "first_name", "last_name")
