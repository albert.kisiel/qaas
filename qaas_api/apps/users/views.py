from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny

from apps.users.serializers import QuizUserRegistrationSerializer
from apps.users.models import QuizUser


class QuizUserCreateView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = QuizUserRegistrationSerializer
    queryset = QuizUser.objects.all()
