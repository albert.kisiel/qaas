from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import redirect
from django.views import View
from django.urls import reverse

from apps.reports.models import UsageReport


class GenerateUsageReportView(UserPassesTestMixin, View):
    def test_func(self):
        return self.request.user.has_perm("reports.change_report")

    def post(self, request, *args, **kwargs):
        obj, created = UsageReport.generate_for_timeframe()
        messages.add_message(
            request,
            messages.SUCCESS,
            f"{'Fresh' if created else 'Updated'} usage report is ready!",
        )
        return redirect(reverse("admin:reports_usagereport_changelist"))
