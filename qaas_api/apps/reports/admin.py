from django.contrib import admin

from import_export.admin import ExportActionModelAdmin

from apps.reports.models import UsageReport

ALL_USAGE_REPORT_FIELDS = tuple([field.name for field in UsageReport._meta.get_fields()])


@admin.register(UsageReport)
class UsageReportAdmin(ExportActionModelAdmin):
    change_list_template = "admin_templates/base_site.html"
    list_display = ALL_USAGE_REPORT_FIELDS
    readonly_fields = ALL_USAGE_REPORT_FIELDS
