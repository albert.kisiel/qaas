from django.urls import path, include

from apps.reports.views import GenerateUsageReportView

app_name = "quizzes"

urlpatterns = [path("generate-usage-report", GenerateUsageReportView.as_view(), name="generate-usage-report")]
