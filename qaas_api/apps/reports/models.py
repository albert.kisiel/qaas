from django.db import models
from django.utils import timezone

from apps.quizzes.models import Invitation, QuizParticipant, Quiz, QuizParticipantAnswer
from apps.users.models import QuizUser


class UsageReport(models.Model):
    new_users = models.PositiveIntegerField()
    sent_invitations = models.PositiveIntegerField()
    accepted_invitations = models.PositiveIntegerField()
    new_quizzes = models.PositiveIntegerField()
    provided_answers = models.PositiveIntegerField()

    date_from = models.DateField()
    date_to = models.DateField()

    class Meta:
        unique_together = ("date_from", "date_to")

    def __str__(self):
        return f"Usage Report ({self.date_from} - {self.date_to})"

    @classmethod
    def generate_for_timeframe(cls, date_from=timezone.now().date(), date_to=timezone.now().date()):
        created_at_kwargs = {"created_at__date__gte": date_from, "created_at__date__lte": date_to}
        obj, created = cls.objects.update_or_create(
            date_from=date_from,
            date_to=date_to,
            defaults={
                "new_users": QuizUser.objects.filter(
                    date_joined__date__gte=date_from, date_joined__date__lte=date_to
                ).count(),
                "sent_invitations": Invitation.objects.filter(
                    sent_at__date__gte=date_from, sent_at__date__lte=date_to
                ).count(),
                "accepted_invitations": QuizParticipant.objects.filter(**created_at_kwargs).count(),
                "new_quizzes": Quiz.objects.filter(**created_at_kwargs).count(),
                "provided_answers": QuizParticipantAnswer.objects.filter(**created_at_kwargs).count(),
            },
        )
        return obj, created
